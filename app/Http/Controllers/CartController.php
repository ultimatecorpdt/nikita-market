<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CartController extends Controller
{
    public function panier_page(){
        $title = 'Panier Page';
        return view('cart')->with([
            'title'=>$title,
        ]);
    }
}
