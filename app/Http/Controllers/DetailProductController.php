<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\categorieProduit;
use App\produit;
use DateTime;

class DetailProductController extends Controller
{
    //
    public function detail_page($id){
        $title = 'Detail Page';
        $produit = produit::findOrFail($id);
        return view('detail_produit')->with([
            'title'=>$title,
            'produit'=>$produit,
        ]);
    }

}
