<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    //INDEX CONTROLLER FOR ALL ACTIONS BEGIN
    public function index_page(){
        $title = 'Index Page';
        return view('index')->with([
            'title'=>$title,
        ]);
    }
}
