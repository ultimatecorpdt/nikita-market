<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PaiementController extends Controller
{
    //FUNCTION FOR PAIEMENT
    public function paiement_page(){
        $title = 'Paiement Page';

        return view('paiement')->with([
            'title'=>$title,
        ]);
    }
}
