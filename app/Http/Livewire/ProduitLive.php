<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\categorieProduit;
use App\produit;
use DateTime;
use Gloudemans\Shoppingcart\Facades\Cart;
use Livewire\WithPagination;

class ProduitLive extends Component
{
    use WithPagination;
    // public $lesproduits;
    public $detailproduit;

    public $test = 10;
    public $designation = '';
    public $url_image1 = '250x250.png';
    public $url_image2 = '250x250.png';
    public $url_image3 = '250x250.png';
    public $poids = '';
    public $description = '';
    public $dateFabr = '';
    public $dateExp = '';
    public $priceUT = 25;

    public $nombrecart = 0;


    public function mount(){
        $this->detailproduit = produit::orderBy('id', 'asc')->first();
        // $this->detailproduit='';
        // $this->load_produits();
        $this->url_image1 = '250x250.png';
        $this->url_image2 = '250x250.png';
        $this->url_image3 = '250x250.png';
    }

    // public function load_produits(){
    //     $this->lesproduits = produit::orderBy('id', 'asc')->paginate(5);;
    // }

    public function on($id){
        $this->test = 20;
    }

    public function load_item_produit($id){
        $this->detailproduit = produit::findOrFail($id);
        // dd($this->detailproduit);
        // alert('Name updated to: ' + $this->detailproduit->designation);
    }

    public function add_to_cart($id){

        $produit = produit::findorFail($id);
        // dd($produit);
        $piods = (int)$produit->poids;
        Cart::add($produit->id, $produit->designation, 1, $produit->priceUT, $piods);
        // Cart::destroy();
        $this->nombrecart = Cart::content()->count();

        // $detail = Cart::content();
        // dd($detail);
    }

    public function render()
    {
        return view('livewire.produit-live',[
            'lesproduits' => produit::orderBy('id', 'asc')->paginate(5),
        ]);
    }



}
