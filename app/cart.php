<?php

namespace App;
use App\produit;
use Illuminate\Database\Eloquent\Model;

class cart extends Model
{
    //
    public function produit()
    {
        return $this->belongsTo(produit::class);
    }
}
