<?php

namespace App;
use App\produit;
use Illuminate\Database\Eloquent\Model;

class categorieProduit extends Model
{
    //
    public function produits()
    {
    	return $this->hasMany(produit::class);
    }
}
