<?php

namespace App;
use App\User;
use App\linecommande;
use App\facture;
use App\paiement;
use Illuminate\Database\Eloquent\Model;

class commande extends Model
{
    //
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function linecommandes()
    {
    	return $this->hasMany(linecommande::class);
    }

    public function factures()
    {
    	return $this->hasMany(facture::class);
    }

    public function paiements()
    {
    	return $this->hasMany(paiement::class);
    }
}
