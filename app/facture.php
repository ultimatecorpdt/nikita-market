<?php

namespace App;
use App\User;
use App\commande;
use App\paiement;
use Illuminate\Database\Eloquent\Model;

class facture extends Model
{
    //
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function commande()
    {
        return $this->belongsTo(commande::class);
    }

    public function paiement()
    {
        return $this->belongsTo(paiement::class);
    }
}
