<?php

namespace App;
use App\commande;
use App\produit;
use Illuminate\Database\Eloquent\Model;

class linecommande extends Model
{
    //
    public function commande()
    {
        return $this->belongsTo(commande::class);
    }

    public function produit()
    {
        return $this->belongsTo(produit::class);
    }
}
