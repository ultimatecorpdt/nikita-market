<?php

namespace App;
use App\facture;
use App\User;
use App\commande;
use Illuminate\Database\Eloquent\Model;

class paiement extends Model
{
    //
    public function factures()
    {
    	return $this->hasMany(facture::class);
    }

    public function commande()
    {
        return $this->belongsTo(commande::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

}
