<?php

namespace App;
use App\categorieProduit;
use App\devise;
use App\linecommande;
use App\cart;
use Illuminate\Database\Eloquent\Model;

class produit extends Model
{
    //
    public function categorieProduit()
    {
        return $this->belongsTo(categorieProduit::class);
    }

    public function devise()
    {
        return $this->belongsTo(devise::class);
    }

    public function linecommandes()
    {
    	return $this->hasMany(linecommande::class);
    }

    public function carts()
    {
    	return $this->hasMany(cart::class);
    }


}
