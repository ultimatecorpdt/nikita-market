<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->email,
        'email_verified_at' => now(),
        'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
        'remember_token' => Str::random(10),
        'phone' => $faker->e164PhoneNumber,
        'username' => $faker->userName,
        'adresse' => $faker->streetAddress,
        'city' => $faker->city,
        'pays' => $faker->country,
        'creditcard' => $faker->creditCardNumber,
        'creditcardType' => $faker->creditCardType,
        'cardExpMo' => $faker->dayOfMonth($max = 'now'),
        'cardExpYr' => $faker->year($max = 'now'),
        'ipAdress' => $faker->ipv4,

    ];
});
