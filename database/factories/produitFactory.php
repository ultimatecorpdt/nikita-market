<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\produit;
use Faker\Generator as Faker;

$factory->define(produit::class, function (Faker $faker) {
    return [
        'designation' => $faker->foodName(),
        'description' => $faker->sentence($nbWords = 6, $variableNbWords = true),
    ];
});
