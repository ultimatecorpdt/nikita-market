<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProduitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('produits', function (Blueprint $table) {
            $table->increments('id');
            $table->string('designation');
            $table->string('description');
            $table->decimal('priceUT');
            $table->string('dispoibleFormat');
            $table->integer('quantite');
            $table->string('image_url1');
            $table->string('image_url2');
            $table->string('image_url3');
            $table->string('poids');
            $table->dateTime('dateExp');
            $table->dateTime('dateFabr');
            $table->string('marque');
            $table->string('lieuFabr');
            $table->string('paysVente');
            $table->boolean('is_active')->default(0);
            $table->boolean('is_new')->default(0);
            $table->boolean('is_veg')->default(0);
            $table->boolean('is_Popular')->default(0);

            $table->integer('categories_id')->unsigned()->index()->nullable();
            $table->timestamps();

            $table->foreign('categories_id')->references('id')->on('categorie_produits')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('produits');
    }
}
