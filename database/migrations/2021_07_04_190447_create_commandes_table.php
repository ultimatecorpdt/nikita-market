<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommandesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('commandes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('reference');
            $table->decimal('montant');
            $table->dateTime('dateCommande');
            $table->enum('status', ['Delivered', 'pending', 'cancel']);
            $table->string('description');
            $table->boolean('is_active')->default(0);
            $table->string('token');
            $table->integer('users_id')->unsigned()->index()->nullable();
            $table->timestamps();

            $table->foreign('users_id')->references('id')->on('users')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('commandes');
    }
}
