<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLinecommandesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('linecommandes', function (Blueprint $table) {
            $table->increments('id');
            $table->decimal('prix');
            $table->integer('quantite');
            $table->dateTime('date');
            $table->boolean('is_active')->default(0);
            $table->integer('commandes_id')->unsigned()->index()->nullable();
            $table->integer('produits_id')->unsigned()->index()->nullable();
            $table->integer('devises_id')->unsigned()->index()->nullable();
            $table->timestamps();

            $table->foreign('commandes_id')->references('id')->on('commandes')->onDelete('set null');
            $table->foreign('produits_id')->references('id')->on('produits')->onDelete('set null');
            $table->foreign('devises_id')->references('id')->on('devises')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('linecommandes');
    }
}
