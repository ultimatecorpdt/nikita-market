<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaiementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('paiements', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('transactionStatus', ['success', 'pending', 'cancel']);
            $table->dateTime('date');
            $table->decimal('montant');
            $table->boolean('is_active')->default(0);
            $table->integer('users_id')->unsigned()->index()->nullable();
            $table->integer('commandes_id')->unsigned()->index()->nullable();
            $table->timestamps();

            $table->foreign('users_id')->references('id')->on('users')->onDelete('set null');
            $table->foreign('commandes_id')->references('id')->on('commandes')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('paiements');
    }
}
