<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFacturesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('factures', function (Blueprint $table) {
            $table->increments('id');
            $table->string('reference');
            $table->dateTime('date');
            $table->boolean('is_active')->default(0);
            $table->integer('commandes_id')->unsigned()->index()->nullable();
            $table->integer('paiements_id')->unsigned()->index()->nullable();
            $table->integer('users_id')->unsigned()->index()->nullable();
            $table->timestamps();
            $table->foreign('commandes_id')->references('id')->on('commandes')->onDelete('set null');
            $table->foreign('paiements_id')->references('id')->on('paiements')->onDelete('set null');
            $table->foreign('users_id')->references('id')->on('users')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('factures');
    }
}
