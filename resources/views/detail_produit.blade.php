<!DOCTYPE html>
<html lang="en">
<head>
	<title>{{$title}}</title>
	<meta name="format-detection" content="telephone=no">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<link href="https://fonts.googleapis.com/css?family=Montserrat%7CRoboto:100,300,400,500,700,900%7CRoboto+Condensed:100,300,400,500,700" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="{{ asset('css/icon-font-linea.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/multirange.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap-theme.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/themify-icons.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/effect.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/font-awesome.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/category.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/owl.theme.default.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/owl.carousel.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/responsive.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/product.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/slick.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/slick-theme.css') }}">
</head>
<body>
	    <!-- push menu-->
    <div class="pushmenu menu-home5">
        <div class="menu-push">
            <span class="close-left js-close"><i class="fa fa-times f-20"></i></span>
            <div class="clearfix"></div>
            <form role="search" method="get" id="searchform" class="searchform" action="/search">
                <div>
                    <label class="screen-reader-text" for="q"></label>
                    <input type="text" placeholder="Search for products" value="" name="q" id="q" autocomplete="off">
                    <input type="hidden" name="type" value="product">
                    <button type="submit" id="searchsubmit"><i class="ion-ios-search-strong"></i></button>
                </div>
            </form>
            <ul class="nav-home5 js-menubar">
                <li class="level1 active dropdown">
                    <a href="#">Home</a>
                    <span class="icon-sub-menu"></span>
                    <ul class="menu-level1 js-open-menu">
                        <li class="level2"><a href="home_v1.html" title="">Home 1</a></li>
                        <li class="level2"><a href="home_v2.html" title="">Home 2</a></li>
                        <li class="level2"><a href="home_v3.html" title="">Home 3</a></li>
                    </ul>
                </li>
                <li class="level1 active dropdown"><a href="#">Shop</a>
                    <span class="icon-sub-menu"></span>
                    <div class="menu-level1 js-open-menu">
                        <ul class="level1">
                            <li class="level2">
                                <a href="#">Shop Type</a>
                                <ul class="menu-level-2">
                                    <li class="level3"><a href="category_v1.html" title="">Category V1</a></li>
                                    <li class="level3"><a href="category_v1_2.html" title="">Category V1.2</a></li>
                                    <li class="level3"><a href="category_v2.html" title="">Category V2</a></li>
                                    <li class="level3"><a href="category_v2_2.html" title="">Category V2.2</a></li>
                                    <li class="level3"><a href="category_v3.html" title="">Category V3</a></li>
                                    <li class="level3"><a href="category_v3_2.html" title="">Category V3.2</a></li>
                                    <li class="level3"><a href="category_v4.html" title="">Category V4</a></li>
                                    <li class="level3"><a href="category_v4_2.html" title="">Category V4.2</a></li>
                                </ul>
                            </li>
                            <li class="level2">
                                <a href="#">Single Product Type</a>
                                <ul class="menu-level-2">
                                    <li class="level3"><a href="product_v1.html" title="">Product Single 1</a></li>
                                    <li class="level3"><a href="product_v2.html" title="">Product Single 2</a></li>
                                    <li class="level3"><a href="product_v3.html" title="">Product Single 3</a></li>
                                </ul>
                            </li>
                            <li class="level2">
                                <a href="#">Order Page</a>
                                <ul class="menu-level-2">
                                    <li class="level3"><a href="cartpage.html" title="">Cart Page</a></li>
                                    <li class="level3"><a href="checkout.html" title="">Checkout</a></li>
                                    <li class="level3"><a href="compare.html" title="">Compare</a></li>
                                    <li class="level3"><a href="quickview.html" title="">Quickview</a></li>
                                    <li class="level3"><a href="trackyourorder.html">Track Order</a></li>
                                    <li class="level3"><a href="wishlist.html">WishList</a></li>
                                </ul>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                </li>
                <li class="level1">
                    <a href="#">Pages</a>
                    <span class="icon-sub-menu"></span>
                    <ul class="menu-level1 js-open-menu">
                        <li class="level2"><a href="about.html" title="About Us ">About Us </a></li>
                        <li class="level2"><a href="contact.html" title="Contact">Contact</a></li>
                        <li class="level2"><a href="404.html" title="404">404</a></li>
                    </ul>
                </li>
                <li class="level1">
                    <a href="#">Blog</a>
                    <span class="icon-sub-menu"></span>
                    <ul class="menu-level1 js-open-menu">
                        <li class="level2"><a href="blog.html" title="Blog Standar">Blog Category</a></li>
                        <li class="level2"><a href="blogdetail.html" title="Blog Gird">Blog Detail</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
    <!-- end push menu-->
	<!-- Menu Mobile -->
	<div class="menu-mobile-left-content menu-bg-white">
		<ul>
			<li><a href="#"><img src="{{asset('img/icon_hot_gray.png')}}" alt="Icon Hot Deals" /> <p>Hot Deals</p></a></li>
			<li><a href="#"><img src="{{asset('img/icon_food_gray.png')}}" alt="Icon Food" /> <p>Food</p></a></li>
			<li><a href="#"><img src="{{asset('img/icon_mobile_gray.png')}}" alt="Icon Mobile & Tablet" /> <p>Mobile & Tablet</p></a></li>
			<li><a href="#"><img src="{{asset('img/icon_electric_gray.png')}}" alt="Icon Electric Appliances" /> <p>Electric Appliances</p></a></li>
			<li><a href="#"><img src="{{asset('img/icon_computer_gray.png')}}" alt="Icon Electronics & Technology" /> <p>Electronics & Technology</p></a></li>
			<li><a href="#"><img src="{{asset('img/icon_fashion_gray.png')}}" alt="Icon Fashion" /> <p>Fashion</p></a></li>
			<li><a href="#"><img src="{{asset('img/icon_health_gray.png')}}" alt="Icon Health & Beauty" /> <p>Health & Beauty</p></a></li>
			<li><a href="#"><img src="{{asset('img/icon_mother_gray.png')}}" alt="Icon Mother & Baby" /> <p>Mother & Baby</p></a></li>
			<li><a href="#"><img src="{{asset('img/icon_book_gray.png')}}" alt="Icon Books & Stationery" /> <p>Books & Stationery</p></a></li>
			<li><a href="#"><img src="{{asset('img/icon_home_gray.png')}}" alt="Icon Home & Life" /> <p>Home & Life</p></a></li>
			<li><a href="#"><img src="{{asset('img/icon_sport_gray.png')}}" alt="Icon Sports & Outdoors" /> <p>Sports & Outdoors</p></a></li>
			<li><a href="#"><img src="{{asset('img/icon_auto_gray.png')}}" alt="Icon Auto & Moto" /> <p>Auto & Moto</p></a></li>
			<li><a href="#"><img src="{{asset('img/icon_voucher_gray.png')}}" alt="Icon Voucher Service" /> <p>Voucher Service</p></a></li>
		</ul>
	</div>
	<!-- Header Box -->
	<div class="wrappage">
		<div class="wrappage">
			<!-- Header Box -->
			<header class="relative full-width">
				<div class=" container-web relative">
					<div class=" container">
						<div class="row">
							<div class=" header-top">
								<p class="contact_us_header col-md-4 col-xs-12 col-sm-3 clear-margin">
									<img src="{{asset('img/icon_phone_top.png')}}" alt="Icon Phone Top Header" /> Contact nous <span class="text-red bold">+243 824202032</span>
								</p>
								<div class="menu-header-top text-right col-md-8 col-xs-12 col-sm-6 clear-padding">
									<ul class="clear-margin">
										<li class="relative"><a href="#">Mon Compte</a></li>
										<li class="relative">
											<a href="#">FR</a>
											<ul>
												<li class="relative"><a href="#">EN</a></li>
												<li class="relative"><a href="#">LG</a></li>
											</ul>
										</li>
										<li class="relative">
											<a href="#">USD</a>
											<ul>
												<li class="relative"><a href="#">CDF</a></li>
												<li class="relative"><a href="#">EUR</a></li>
											</ul>
										</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="clearfix header-content full-width relative">
								<div class="clearfix icon-menu-bar">
									<i class="data-icon data-icon-arrows icon-arrows-hamburger-2 icon-pushmenu js-push-menu"></i>
								</div>
								<div class="clearfix logo">
									<a href="#"><img alt="Logo" src="{{asset('img/01LOGO.png')}}" width="200" height="42"/></a>
								</div>
								<div class="clearfix search-box relative float-left">
									<form method="GET" action="/" class="">
										<div class="clearfix category-box relative">
											<select name="cate_search">
												<option>Les Categories</option>
												<option>Agri-culture</option>
												<option>Alimentaire</option>
												<option>Elevage</option>
												<option>pêche</option>
											</select>
										</div>
										<input type="text" name="s" placeholder="Entrez votre recherche ici . . .">
										<button type="submit" class="animate-default button-hover-red">search</button>
									</form>
								</div>
								<div class="clearfix icon-search-mobile absolute">
									<i onclick="showBoxSearchMobile()" class="data-icon data-icon-basic icon-basic-magnifier"></i>
								</div>
								<div class="clearfix cart-website absolute" onclick="showCartBoxDetail()">
									<img alt="Icon Cart" src="{{asset('img/icon_cart.png')}}" />
									<p class="count-total-shopping absolute">2</p>
								</div>
								<div class="clearfix cart-website absolute" onclick="showCartBoxDetail()">
									<img alt="Icon Cart" src="{{asset('img/icon_cart.png')}}" />
									<p class="count-total-shopping absolute">3</p>
								</div>
								<div class="cart-detail-header border">
									<div class="relative">
										<div class="product-cart-son clearfix">
											<div class="image-product-cart float-left center-vertical-image ">
												<a href="#"><img src="{{asset('img/3-1.jpg')}}" alt="" /></a>
											</div>
											<div class="info-product-cart float-left">
												<p class="title-product title-hover-black"><a class="animate-default" href="#">Sac de Riz</a></p>
												<p class="clearfix price-product">$50 <span class="total-product-cart-son">(x1)</span></p>
											</div>
										</div>
										<div class="product-cart-son">
											<div class="image-product-cart float-left center-vertical-image">
												<a href="#"><img src="{{asset('img/Broli_green_peas-02.jpg')}}" alt="" /></a>
											</div>
											<div class="info-product-cart float-left">
												<p class="title-product title-hover-black"><a class="animate-default" href="#">Boite Haricot</a></p>
												<p class="clearfix price-product">$35 <span class="total-product-cart-son">(x1)</span></p>
											</div>
										</div>
									</div>
									<div class="relative border no-border-l no-border-r total-cart-header">
										<p class="bold clear-margin">Subtotal:</p>
										<p class=" clear-margin bold">$85</p>
									</div>
									<div class="relative btn-cart-header">
										<a href="/panier" class="uppercase bold animate-default">view cart</a>
										<a href="#" class="uppercase bold button-hover-red animate-default">Annuler</a>
									</div>
								</div>
								<div class="mask-search absolute clearfix" onclick="hiddenBoxSearchMobile()"></div>
								<div class="clearfix box-search-mobile">
								</div>
							</div>
						</div>
						<div class="row">
							<a class="menu-vertical hidden-md hidden-lg" onclick="showMenuMobie()">
								<span class="animate-default"><i class="fa fa-list" aria-hidden="true"></i> Les categories</span>
							</a>
						</div>
					</div>
				</div>
				<div class="menu-header-v3 hidden-ipx">
					<div class="container">
						<div class="row">
							<!-- Menu Page -->
							<div class="menu-header full-width">
								<ul class="clear-margin">
									<li onclick="showMenuHomeV3()"><a class="animate-default" href="#">
										<i class="fa fa-list" aria-hidden="true"></i> Nos categories</a>
									</li>
									<li class="title-hover-red">
										<a class="animate-default" href="#">home</a>
									</li>
									<li class="title-hover-red">
										<a class="animate-default" href="#">Boutique</a>
									</li>
									<li class="title-hover-red">
										<a class="animate-default" href="#">Nikita market</a>
										<ul>
											<li class="title-hover-red"><a class="animate-default" href="about.html">A propos</a></li>
											<li class="title-hover-red"><a class="animate-default" href="contact.html">Contact Nous</a></li>
										</ul>
									</li>
									<li onclick="showMenuHomeV3()"><a class="animate-default" href="#">
										<img src="{{asset('img/zget-free.gif')}}" alt="" style="position: absolute;top: -1em;left: 25em;">
									</li>
								</ul>
							</div>
							<!-- End Menu Page -->

						</div>
					</div>
				</div>
				<div class="clearfix menu_more_header menu-web menu-bg-white">
					<ul>
						<li><a href="#"><img src="{{asset('img/icon_hot_gray.png')}}" alt="Icon Hot Deals" /> <p>Agri-culture</p></a></li>
						<li><a href="#"><img src="{{asset('img/icon_food_gray.png')}}" alt="Icon Food" /> <p>Alimentaire</p></a></li>
						<li><a href="#"><img src="{{asset('img/icon_mobile_gray.png')}}" alt="Icon Mobile & Tablet" /> <p>Elevage</p></a></li>
						<li><a href="#"><img src="{{asset('img/icon_mobile_gray.png')}}" alt="Icon Mobile & Tablet" /> <p>Pêche</p></a></li>
					</ul>
				</div>
				<div class="header-ontop">
					<div class="container">
						<div class="row">
							<div class="col-md-3">
								<div class="clearfix logo">
									<a href="#"><img alt="Logo" src="{{asset('img/01LOGO.png')}}" width="200" height="42"/></a>
								</div>
							</div>
							<div class="col-md-9">
								<div class="menu-header">
									<ul class="main__menu clear-margin">
										<li class="title-hover-red">
											<a class="animate-default" href="/">home</a>
										</li>
										<li class="title-hover-red">
											<a class="animate-default" href="/">Boutique</a>
										</li>
										<li class="title-hover-red">
											<a class="animate-default" href="#">Nikita market</a>
											<ul>
												<li class="title-hover-red"><a class="animate-default" href="about.html">A propos</a></li>
												<li class="title-hover-red"><a class="animate-default" href="contact.html">Contact Nous</a></li>
											</ul>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</header>
			<!-- End Header Box -->
			<!-- Content Box -->
			<div class="relative full-width">
				<!-- Breadcrumb -->
				<div class="container-web relative">
					<div class="container">
						<div class="row">
							<div class="breadcrumb-web">
								<ul class="clear-margin">
									<li class="animate-default title-hover-red"><a href="/">Home</a></li>
									<li class="animate-default title-hover-red"><a href="#">{{$produit->designation}}</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<!-- End Breadcrumb -->
				<!-- Content Category -->
				<div class="relative container-web">
					<div class="container">
						<div class="row ">
							<!-- Sider Bar -->
							<div class="col-md-3 relative right-padding-default clear-padding" id="slide-bar-category">
								<div class="col-md-12 col-sm-12 col-xs-12 sider-bar-category border bottom-margin-default">
									<p class="title-siderbar bold">CATEGORIES</p>
									<ul class="clear-margin list-siderbar">
										<li><a href="#">Alimentaire</a></li>
										<li><a href="#">Agri-culture</a></li>
										<li><a href="#">Elevage</a></li>
										<li><a href="#">Pêche</a></li>
									</ul>
								</div>
								<!-- Element Best Sellers -->
								<div class="col-sm-12 col-sm-12 col-xs-12 sider-bar-category border bottom-margin-default">
									<p class="title-siderbar bold bottom-margin-15-default">BEST SELLERS</p>
									<div class="clearfix relative best-sellers-product">
										<div class="image-product-sellers-sidebar float-left">
											<a href="#"><img src="{{asset('img/product_image_6-min.png')}}" alt="" /></a>
										</div>
										<div class="info-product-sellers-sidebar float-left">
											<p class="title-product-sellers-sidebar title-hover-black"><a class="animate-default" href="#">MH02-Black09</a></p>
											<div class="clearfix ranking-product-sidebar ranking-color">
												<i class="fa fa-star" aria-hidden="true"></i>
												<i class="fa fa-star" aria-hidden="true"></i>
												<i class="fa fa-star" aria-hidden="true"></i>
												<i class="fa fa-star-half" aria-hidden="true"></i>
												<i class="fa fa-star-o" aria-hidden="true"></i>
											</div>
											<p class="clearfix price-product"><span class="price-old">$700</span> $350</p>
										</div>
									</div>
									<div class="clearfix relative best-sellers-product">
										<div class="image-product-sellers-sidebar float-left">
											<a href="#"><img src="{{asset('img/product_image_7-min.png')}}" alt="" /></a>
										</div>
										<div class="info-product-sellers-sidebar float-left">
											<p class="title-product-sellers-sidebar title-hover-black"><a class="animate-default" href="#">Voyage Bag</a></p>
											<div class="clearfix ranking-product-sidebar ranking-color">
												<i class="fa fa-star" aria-hidden="true"></i>
												<i class="fa fa-star" aria-hidden="true"></i>
												<i class="fa fa-star" aria-hidden="true"></i>
												<i class="fa fa-star-half" aria-hidden="true"></i>
												<i class="fa fa-star-o" aria-hidden="true"></i>
											</div>
											<p class="clearfix price-product"><span class="price-old">$700</span> $350</p>
										</div>
									</div>
									<div class="clearfix relative best-sellers-product">
										<div class="image-product-sellers-sidebar float-left">
											<a href="#"><img src="{{asset('img/product_image_8-min.png')}}" alt="" /></a>
										</div>
										<div class="info-product-sellers-sidebar float-left">
											<p class="title-product-sellers-sidebar title-hover-black"><a class="animate-default" href="#">Impulse</a></p>
											<div class="clearfix ranking-product-sidebar ranking-color">
												<i class="fa fa-star" aria-hidden="true"></i>
												<i class="fa fa-star" aria-hidden="true"></i>
												<i class="fa fa-star" aria-hidden="true"></i>
												<i class="fa fa-star-half" aria-hidden="true"></i>
												<i class="fa fa-star-o" aria-hidden="true"></i>
											</div>
											<p class="clearfix price-product"><span class="price-old">$700</span> $350</p>
										</div>
									</div>
								</div>
								<!-- End Element Best Sale -->
								<!-- Element On Sale -->
								<div class="col-sm-12 col-sm-12 col-xs-12 sider-bar-category border bottom-margin-default">
									<p class="title-siderbar bold bottom-margin-default">ON SALE</p>
									<div class="slide-on-sale-sidebar relative">
										<div class="owl-theme owl-carousel">
											<div class="items">
												<div class="product-category relative">
													<p class="absolute label-on-sale">-50%<br>off</p>
													<div class="image-product relative overfollow-hidden">
														<img src="{{asset('img/product_image_4-min.png')}}" alt="Product">
														<a href="#"></a>
														<ul class="option-product animate-default">
															<li class="relative"><a href="#"><i class="data-icon data-icon-ecommerce icon-ecommerce-bag"></i></a></li>
															<li class="relative"><a href="#"><i class="data-icondata-icon-basic icon-basic-heart" aria-hidden="true"></i></a></li>
															<li class="relative"><a href="javascript:;" ><i class="data-icon data-icon-basic icon-basic-magnifier" aria-hidden="true"></i></a></li>
														</ul>
													</div>
													<p class="title-product clearfix full-width animate-default title-hover-black"><a href="#">MH01-Black</a></p>
													<p class="clearfix price-product"><span class="price-old">$700</span> $350</p>
													<div class="clearfix ranking-product-category ranking-color">
														<i class="fa fa-star" aria-hidden="true"></i>
														<i class="fa fa-star" aria-hidden="true"></i>
														<i class="fa fa-star" aria-hidden="true"></i>
														<i class="fa fa-star-half" aria-hidden="true"></i>
														<i class="fa fa-star-o" aria-hidden="true"></i>
													</div>
													<a href="javascript:;" class="button-add-cart-on-sale button-hover-red animate-default">Add to Cart</a>
												</div>
											</div>
											<div class="items">
												<div class="product-category relative">
													<p class="absolute label-on-sale">-42%<br>off</p>
													<div class="image-product relative overfollow-hidden">
														<img src="{{asset('img/product_home_26-min.png')}}" alt="Product">
														<a href="#"></a>
														<ul class="option-product animate-default">
															<li class="relative"><a href="#"><i class="data-icon data-icon-ecommerce icon-ecommerce-bag"></i></a></li>
															<li class="relative"><a href="#"><i class="data-icondata-icon-basic icon-basic-heart" aria-hidden="true"></i></a></li>
															<li class="relative"><a href="javascript:;" ><i class="data-icon data-icon-basic icon-basic-magnifier" aria-hidden="true"></i></a></li>
														</ul>
													</div>
													<p class="title-product clearfix full-width animate-default title-hover-black"><a href="#">Impulse Duffle</a></p>
													<p class="clearfix price-product"><span class="price-old">$700</span> $350</p>
													<div class="clearfix ranking-product-category ranking-color">
														<i class="fa fa-star" aria-hidden="true"></i>
														<i class="fa fa-star" aria-hidden="true"></i>
														<i class="fa fa-star" aria-hidden="true"></i>
														<i class="fa fa-star-half" aria-hidden="true"></i>
														<i class="fa fa-star-o" aria-hidden="true"></i>
													</div>
													<a href="javascript:;" class="button-add-cart-on-sale button-hover-red animate-default">Add to Cart</a>
												</div>
											</div>
										</div>
									</div>
								</div>
								<!-- End ELement On Sale -->
								<!-- <div class="bottom-margin-default banner-siderbar col-md-12 col-sm-12 col-xs-12 clear-padding clearfix">
									<div class="overfollow-hidden banners-effect5 relative">
										<img src="{{asset('img/banner_siderbar-min.png')}}" alt="Siderbar" />
										<a href="#"></a>
									</div>
								</div> -->
							</div>
							<!-- End Sider Bar Box -->
							<!-- Content Category -->
							<div class="col-md-9 relative clear-padding">
								<div class="col-sm-12 col-xs-12 col-md-1 relative overfollow-hidden clear-padding button-show-sidebar clearfix">
									<p onclick="showSideBar()"><span class="ti-filter"></span> Sidebar</p>
								</div>
								<!-- Product Content Detail -->
								<div class="top-product-detail relative ">
									<div class="row">
										<!-- Slide Product Detail -->
										<div class="col-md-7 relative col-sm-12 col-xs-12">
											<div id="owl-big-slide" class="relative sync-owl-big-image">
												<div class="item center-vertical-image">
													<img src="{{asset('img/'.$produit->image_url1)}}" alt="Image Big Slide">
												</div>
											</div>
										</div>
										<!-- Info Top Product -->
										<div class="col-md-5 col-sm-12 col-xs-12">
											<div class="name-ranking-product relative bottom-padding-15-default bottom-margin-15-default border no-border-r no-border-t no-border-l">
												<h1 class="name-product">{{$produit->designation}}</h1>
												<div class=" ranking-color ">
													<i class="fa fa-star" aria-hidden="true"></i>
													<i class="fa fa-star" aria-hidden="true"></i>
													<i class="fa fa-star" aria-hidden="true"></i>
													<i class="fa fa-star-half" aria-hidden="true"></i>
													<i class="fa fa-star-o" aria-hidden="true"></i>
												</div>
												<p class="clearfix price-product">{{$produit->priceUT}}</p>
												<div class="product-code clearfix full-width">
													<p class="float-left relative">Poids: {{$produit->poids}}</p>
													<p class="float-left clear-margin">Availability: <span class="text-green">In stock</span></p>
												</div>
											</div>
											<div class="relative intro-product-detail bottom-margin-15-default bottom-padding-15-default border no-border-r no-border-t no-border-l">
												<p class="clear-margin">{{$produit->description}}</p>
											</div>
											<div class="relative option-product-detail bottom-padding-15-default border no-border-r no-border-t no-border-l">
												<p class="bold clear-margin bottom-margin-15-default">Available Options:</p>
												<div class="relative option-product-1 bottom-margin-15-default">
													<p class="float-left">Color:</p>
													<ul class="check-box-custom list-color clearfix clear-margin">
														<li>
															<label>
                                                                {{$produit->dispoibleFormat}}
															</label>
														</li>
													</ul>
												</div>
												<div class="relative option-product-2 clearfix">
													<div class="option-product-son float-left right-margin-default">
														<p class="float-left">Qty:</p>
														<input type="number" class="left-margin-15-default" min="01" step="1" max="10" value="1" name="num">
													</div>
													<div class="option-product-son float-left">
														<p class="float-left">Size</p>
														<select class="">
															<option value="x">X</option>
															<option value="s">S</option>
															<option value="xl">XL</option>
															<option value="xxl">XXL</option>
														</select>
													</div>
												</div>
											</div>
											<div class="relative button-product-list clearfix full-width clear-margin">
												<ul class="clear-margin top-margin-default clearfix bottom-margin-default">
													<li class="button-hover-red"><a href="#" class="animate-default">Add to Cart</a></li>
													<li><a href="#" class="animate-default"><i class="fa fa-heart" aria-hidden="true"></i></a></li>
													<li><a href="#" class="animate-default"><i class="fa fa-signal" aria-hidden="true"></i></a></li>
													<li><a href="#" class="animate-default"><i class="fa fa-search" aria-hidden="true"></i></a></li>
												</ul>
												<div class="btn-print clearfix">
													<a href="javascript:;" class="right-margin-default animate-default title-hover-black" onclick="printWebsite()"><i class="fa fa-print" aria-hidden="true"></i> Print</a>
													<a href="mailto:" class=" animate-default title-hover-black"><i class="fa fa-envelope" aria-hidden="true"></i> Send to a Friend</a>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="info-product-detail bottom-margin-default relative">
									<div class="row">
										<div class="col-md-12 relative overfollow-hidden">
											<ul class="title-tabs clearfix relative">
												<li onclick="changeTabsProductDetail(1)" class="title-tabs-product-detail title-tabs-1 border no-border-b active-title-tabs bold uppercase">Product Details</li>
												<li onclick="changeTabsProductDetail(2)" class="title-tabs-product-detail title-tabs-2 border no-border-b bold uppercase">Information</li>
												<!-- <li onclick="changeTabsProductDetail(3)" class="title-tabs-product-detail title-tabs-3 border no-border-b bold uppercase">Reviews</li> -->
											</ul>
											<div class="content-tabs-product-detail relative content-tab-1 border active-tabs-product-detail bottom-padding-default top-padding-default left-padding-default right-padding-default">
												<p>Vestibulum eu odio. Suspendisse potenti. Morbi mollis tellus ac sapien. Praesent egestas tristique nibh. Nullam dictum felis eu pede mollis pretium. Fusce egestas elit eget lorem. In auctor lobortis lacus.</p>
												<p>Morbi mollis tellus ac sapien. Nunc nec neque. Praesent nec nisl a purus blandit viverra. Nunc nec neque. Pellentesque auctor neque nec urna. Curabitur suscipit suscipit tellus. Cras id dui. Nam ipsum risus, rutrum vitae, vestibulum eu, molestie vel, lacus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hymenaeos. Maecenas vestibulum mollis diam.</p>
												<p>Vestibulum facilisis, purus nec pulvinar iaculis, ligula mi congue nunc, vitae euismod ligula urna in dolor. Sed lectus. Phasellus leo dolor, tempus non, auctor et, hendrerit quis, nisi. Nam at tortor in tellus interdum sagittis. Pellentesque egestas, neque sit amet convallis pulvinar, justo nulla eleifend augue, ac auctor orci leo non est.</p>
											</div>
											<div class="content-tabs-product-detail relative content-tab-2 border bottom-padding-default top-padding-default left-padding-default right-padding-default">
												<p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from divs 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in div 1.10.32.</p>
												<p>The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. divs 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</p>
											</div>
										</div>
									</div>
								</div>
								<div class="slide-product-bottom relative">
									<div class="row">
										<div class="col-md-12 col-sm-12 col-xs-12 relative bottom slide-related-product">
											<p class="bold title-slide-product-bottom">RELATED PRODUCTS</p>
											<div class="button-slide-related" id="btn-slide-1"></div>
											<div class="owl-theme owl-carousel" data-items="1,2,3">
												<div class="items">
													<div class="full-width product-category relative">
														<div class="image-product  relative overfollow-hidden">
															<div class="center-vertical-image">
																<img src="{{asset('img/product_home_31-min.png')}}" alt="Product">
															</div>
															<a href="#"></a>
															<ul class="option-product animate-default">
																<li class="relative"><a href="#"><i class="data-icon data-icon-ecommerce icon-ecommerce-bag"></i></a></li>
																<li class="relative"><a href="#"><i class="data-icondata-icon-basic icon-basic-heart" aria-hidden="true"></i></a></li>
																<li class="relative"><a href="javascript:;" ><i class="data-icon data-icon-basic icon-basic-magnifier" aria-hidden="true"></i></a></li>
															</ul>
														</div>
														<h3 class="title-product animate-default title-hover-black clearfix full-width"><a href="#">Voyage Yoga Bag</a></h3>
														<p class="clearfix price-product"><span class="price-old">$700</span> $350</p>
														<div class="clearfix ranking-product-category ranking-color">
															<i class="fa fa-star" aria-hidden="true"></i>
															<i class="fa fa-star" aria-hidden="true"></i>
															<i class="fa fa-star" aria-hidden="true"></i>
															<i class="fa fa-star-half" aria-hidden="true"></i>
															<i class="fa fa-star-o" aria-hidden="true"></i>
														</div>
													</div>
												</div>
												<div class="items">
													<div class="full-width product-category relative">
														<div class="image-product  relative overfollow-hidden">
															<div class="center-vertical-image">
																<img src="{{asset('img/product_home_32-min.png')}}" alt="Product">
															</div>
															<a href="#"></a>
															<ul class="option-product animate-default">
																<li class="relative"><a href="#"><i class="data-icon data-icon-ecommerce icon-ecommerce-bag"></i></a></li>
																<li class="relative"><a href="#"><i class="data-icondata-icon-basic icon-basic-heart" aria-hidden="true"></i></a></li>
																<li class="relative"><a href="javascript:;" ><i class="data-icon data-icon-basic icon-basic-magnifier" aria-hidden="true"></i></a></li>
															</ul>
														</div>
														<h3 class="title-product animate-default title-hover-black clearfix full-width"><a href="#">MH02-Black09</a></h3>
														<p class="clearfix price-product"><span class="price-old">$700</span> $350</p>
														<div class="clearfix ranking-product-category ranking-color">
															<i class="fa fa-star" aria-hidden="true"></i>
															<i class="fa fa-star" aria-hidden="true"></i>
															<i class="fa fa-star" aria-hidden="true"></i>
															<i class="fa fa-star-half" aria-hidden="true"></i>
															<i class="fa fa-star-o" aria-hidden="true"></i>
														</div>
													</div>
												</div>
												<div class="items">
													<div class="full-width product-category relative">
														<div class="image-product  relative overfollow-hidden">
															<div class="center-vertical-image">
																<img src="{{asset('img/product_home_33-min.png')}}" alt="Product">
															</div>
															<a href="#"></a>
															<ul class="option-product animate-default">
																<li class="relative"><a href="#"><i class="data-icon data-icon-ecommerce icon-ecommerce-bag"></i></a></li>
																<li class="relative"><a href="#"><i class="data-icondata-icon-basic icon-basic-heart" aria-hidden="true"></i></a></li>
																<li class="relative"><a href="javascript:;" ><i class="data-icon data-icon-basic icon-basic-magnifier" aria-hidden="true"></i></a></li>
															</ul>
														</div>
														<h3 class="title-product animate-default title-hover-black clearfix full-width"><a href="#">Wayfarer Messenger Bag</a></h3>
														<p class="clearfix price-product"><span class="price-old">$700</span> $350</p>
														<div class="clearfix ranking-product-category ranking-color">
															<i class="fa fa-star" aria-hidden="true"></i>
															<i class="fa fa-star" aria-hidden="true"></i>
															<i class="fa fa-star" aria-hidden="true"></i>
															<i class="fa fa-star-half" aria-hidden="true"></i>
															<i class="fa fa-star-o" aria-hidden="true"></i>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<!-- End Product Content Category -->
							</div>
						</div>
					</div>
				</div>
				<!-- End Sider Bar -->
				<!-- Support -->
				<div class=" support-box full-width bg-red support_box_v2">
					<div class="container-web">
						<div class="container">
							<div class="row">
								<div class=" support-box-info relative col-md-3 col-sm-3 col-xs-6">
									<img src="{{asset('img/icon_free_ship_white-min.png')}}" alt="Icon Free Ship" class="absolute" />
									<p>free shipping</p>
									<p>on order over $500</p>
								</div>
								<div class=" support-box-info relative col-md-3 col-sm-3 col-xs-6">
									<img src="{{asset('img/icon_support_white-min.png')}}" alt="Icon Supports" class="absolute">
									<p>support</p>
									<p>life time support 24/7</p>
								</div>
								<div class=" support-box-info relative col-md-3 col-sm-3 col-xs-6">
									<img src="{{asset('img/icon_patner_white-min.png')}}" alt="Icon partner" class="absolute">
									<p>help partner</p>
									<p>help all aspects</p>
								</div>
								<div class=" support-box-info relative col-md-3 col-sm-3 col-xs-6">
									<img src="{{asset('img/icon_phone_table_white-min.png')}}" alt="Icon Phone Tablet" class="absolute">
									<p>contact with us</p>
									<p>+07 (0) 7782 9137</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- End Content Box -->
			<!-- Footer Box -->
			<footer class="relative full-width">
				<div class=" top-footer full-width">
					<div class="clearfix container-web relative">
						<div class=" container">
							<div class="row">
								<div class="clearfix col-md-9 col-sm-12 clear-padding col-xs-12">
									<div class="clearfix text-subscribe">
										<i class="fa fa-envelope-o" aria-hidden="true"></i>
										<p>sign up for newsletter</p>
										<p>Get updates on discount & counpons.</p>
									</div>
									<div class="clearfix form-subscribe">
										<input type="text" name="email-subscribe" placeholder="Enter your email . . .">
										<button class="animate-default button-hover-red">subscribe</button>
									</div>
								</div>
								<div class="clearfix col-md-3 col-sm-12 col-xs-12 clear-padding social-box text-right">
									<a href="#"><img src="{{asset('img/social_tw-min.png')}}" alt="Icon Social Twiter"></a>
									<a href="#"><img src="{{asset('img/social_fa-min.png')}}" alt="Icon Social Facebook"></a>
									<a href="#"><img src="{{asset('img/social_int-min.png')}}" alt="Icon Social Instagram"></a>
									<a href="#"><img src="{{asset('img/social_yt-min.png')}}" alt="Icon Social Youtube" /></a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="clearfix container-web relative">
					<div class=" container clear-padding">
					<div class="row">
					<div class="clearfix col-md-3 col-sm-6 col-xs-12 text-footer">
						<p>mon compte</p>
						<ul class="list-footer">
							<li><a href="#">Mon Compte</a></li>
							<li><a href="#">Login</a></li>
							<li><a href="#">Mon Panier</a></li>
							<li><a href="#">Mes Commandes</a></li>
							<li><a href="#">Mes Achats</a></li>
						</ul>
					</div>
					<div class="clearfix col-md-3 col-sm-6 col-xs-12 text-footer">
						<p>information</p>
						<ul class="list-footer">
							<li><a href="#">Information</a></li>
							<li><a href="#">Historique Commandes</a></li>
							<li><a href="#">Evaluation</a></li>
							<li><a href="#">Privacy Policy</a></li>
							<li><a href="#">Site Map</a></li>
						</ul>
					</div>
					<div class="clearfix col-md-3 col-sm-6 col-xs-12 text-footer">
						<p>Notre services</p>
						<ul class="list-footer">
							<li><a href="#">Livraison domicile</a></li>
							<li><a href="#">Service client</a></li>
							<li><a href="#">Retour & Echange</a></li>
							<li><a href="#">Plus d'achat</a></li>
							<li><a href="#">Terms et condition</a></li>
						</ul>
					</div>
					<div class="clearfix col-md-3 col-sm-6 col-xs-12 text-footer">
						<p>contact us</p>
						<ul class="icon-footer">
							<li><i class="fa fa-home" aria-hidden="true"></i> 109B Kinshasa, Kinshasa, RD Congo</li>
							<li><i class="fa fa-envelope" aria-hidden="true"></i> contact@nikitamarket.com</li>
							<li><i class="fa fa-phone" aria-hidden="true"></i> +243 82-420-20-32</li>
							<li><i class="fa fa-fax" aria-hidden="true"></i> +243 82-420-20-32</li>
							<li><i class="fa fa-clock-o" aria-hidden="true"></i> 24h/24</li>
						</ul>
					</div>
				</div>
					</div>
				</div>
				<div class=" bottom-footer full-width">
					<div class="clearfix container-web">
						<div class=" container">
							<div class="row">
								<div class="clearfix col-md-7 clear-padding copyright">
									<p class="clear-margin">Copyright © 2021 by Ultimate Corporation. All Rights Reserved.</p>
								</div>
								<div class="clearfix footer-icon-bottom col-md-5 float-right clear-padding">
									<div class="icon_logo_footer float-right">
										<img src="{{asset('img/image_payment_footer-min.png')}}" alt="">
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</footer>
		</div>
	</div>
	<!-- End Footer Box -->
    <script src="{{ asset('js/jquery-3.3.1.min.js') }}" defer=""></script>
    <script src="{{ asset('js/bootstrap.min.js') }}" defer=""></script>
    <script src="{{ asset('js/multirange.js') }}" defer=""></script>
    <script src="{{ asset('js/slick.min.js') }}" defer=""></script>
    <script src="{{ asset('js/owl.carousel.min.js') }}" defer=""></script>
    <script src="{{ asset('js/sync_owl_carousel.js') }}" defer=""></script>
    <script src="{{ asset('js/scripts.js') }}" defer=""></script>
</body>
</html>