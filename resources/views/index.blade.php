@extends('shared.layout')

@section('content')
<!-- Content Box -->
	<div class="relative full-width">
		<!-- Breadcrumb -->
		<div class="container-web relative">
			<div class="container">
				<div class="row">
					<div class="breadcrumb-web">
						<ul class="clear-margin">
							<li class="animate-default title-hover-red"><a href="#">Accueil</a></li>
							<li class="animate-default title-hover-red"><a href="#">Nos Produits</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<!-- End Breadcrumb -->
		<!-- Content Category -->
		<div class="relative container-web">
			<div class="container">
				<div class="row">
					<div class="col-md-12 relative clear-padding">
						<div class="col-sm-12 col-xs-12 relative overfollow-hidden clear-padding button-show-sidebar">
							<p onclick="showSideBar()"><span class="ti-filter"></span> Sidebar</p>
						</div>
						<div class="banner-top-category-page bottom-margin-default effect-bubba zoom-image-hover overfollow-hidden relative full-width">
							<img src="{{asset('img/b4.jpg')}}" alt=""/>
							<a href="#"></a>
						</div>
					</div>
				</div>

				<!-- Produits Livewire -->
				<livewire:produit-live></livewire:produit-live>
				<!-- End -->

			</div>
		</div>
		<!-- End Sider Bar -->



		<!-- Support -->
		<div class=" support-box full-width bg-red support_box_v2">
			<div class="container-web">
				<div class=" container">
					<div class="row">
						<div class=" support-box-info relative col-md-3 col-sm-3 col-xs-6">
							<img src="{{asset('img/icon_free_ship_white-min.png')}}" alt="Icon Free Ship" class="absolute" />
							<p>Livraison partout en RDC</p>
							<p>Gracim livre votre commande <br>
								partout en RDC en un temp express.</p>
						</div>
						<div class=" support-box-info relative col-md-3 col-sm-3 col-xs-6">
							<img src="{{asset('img/icon_support_white-min.png')}}" alt="Icon Supports" class="absolute">
							<p>support</p>
							<p>Nos services de communication <br> sont en ligne 24/7</p>
						</div>
						<div class=" support-box-info relative col-md-3 col-sm-3 col-xs-6">
							<img src="{{asset('img/icon_patner_white-min.png')}}" alt="Icon partner" class="absolute">
							<p>Meilleurs Offres</p>
							<p>Abonnez-vous à nos réseaux sociaux <br> pour ne rien rater de nos offres</p>
						</div>
						<div class=" support-box-info relative col-md-3 col-sm-3 col-xs-6">
							<img src="{{asset('img/icon_phone_table_white-min.png')}}" alt="Icon Phone Tablet" class="absolute">
							<p>Contacter nous</p>
							<p>+243 824202032</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<!-- End Content Box -->
@endsection