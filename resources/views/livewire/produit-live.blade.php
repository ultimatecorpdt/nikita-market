<div class="row ">
    <!-- Sider Bar -->
    <div class="col-md-3 relative right-padding-default clear-padding" id="slide-bar-category">
        <div class="bottom-margin-default banner-siderbar col-md-12 col-sm-12 col-xs-12 clear-padding clearfix">
            <div class="overfollow-hidden banners-effect5 relative center-vertical-image">
                <img src="{{asset('img/zmedia.gif')}}" alt="Siderbar" />
                <a href="#"></a>
            </div>
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12 sider-bar-category border bottom-margin-default">
            <p class="title-siderbar bold">MENU</p>
            <ul class="clear-margin list-siderbar">
                <li><a href="#">Home</a></li>
                <li><a href="#">Boutique</a></li>
                <li>
                    <a href="#">Nikita Market</a>
                    <ul>
                        <li class="title-hover-red"><a class="animate-default" href="about.html">A propos</a></li>
                        <li class="title-hover-red"><a class="animate-default" href="contact.html">Contact Nous</a></li>
                    </ul>
                </li>
            </ul>
        </div>

        <div class="col-md-12 col-sm-12 col-xs-12 sider-bar-category border bottom-margin-default">
            <p class="title-siderbar bold">CATEGORIES</p>
            <ul class="clear-margin list-siderbar">
                <li><a href="#">Alimentaire</a></li>
                <li><a href="#">Agri-culture</a></li>
                <li><a href="#">Elevage</a></li>
                <li><a href="#">Pêche</a></li>
            </ul>
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12 relative border bottom-margin-default sider-bar-category">
            <div class="relative border no-border-t no-border-l no-border-r bottom-padding-default">
                <p class="title-siderbar bold">Filtrer Par Prix</p>
                <div class="range-slider">
                    <input value="50" min="0" max="1000" class="multi-range" type="range">
                    <p class="text-range">Prix: <span class="number-range"></span></p>
                </div>
            </div>
        </div>
        <!-- Image de publicité -->
        <!-- <div class="bottom-margin-default banner-siderbar col-md-12 col-sm-12 col-xs-12 clear-padding clearfix">
            <div class="overfollow-hidden banners-effect5 relative center-vertical-image">
                <img src="{{asset('img/FOR WEB-NIKITA-02.jpg')}}" alt="Siderbar" />
                <a href="#"></a>
            </div>
        </div> -->

    </div>
    <!-- End Sider Bar Box -->

    <!-- Content Category view Grid -->

    <div id="viewgrid" class="col-md-9 relative clear-padding" >
        <!-- Product Content Category -->
        <div class="relative clearfix">
            <div class="bar-category bottom-margin-default border no-border-r no-border-l no-border-t">
                <div class="row">
                    <div class="col-md-4 col-sm-4 col-xs-4">
                        <p class="title-category-page clear-margin">Alimentation</p>
                    </div>
                    <div class="col-md-8 col-sm-8 col-xs-8 right-category-bar float-right relative">
                        <p class=" float-left">resultats</p>
                        <a class=" float-left active-view-bar" onclick="showgird()">
                            <span class="icon-bar-category border ti-layout-grid3"></span>
                        </a>
                        <a class=" float-left" onclick="showlist()">
                            <span class="icon-bar-category border ti-layout-list-thumb"></span>
                        </a>
                        <a class="float-left" onclick="showCartBoxDetail()">
                            <img alt="Icon Cart" class="position-relative" src="{{asset('img/icon_cart.png')}}" />
                            <span class="position-absolute top-0 start-100 translate-middle badge rounded-pill" style="color:#fff;background-color:red;">
                                {{$nombrecart}}
                            </span>
                        </a>
                    </div>
                </div>
            </div>
            @foreach ($lesproduits as $item)
            <div id="{{$item->id}}" class="col-md-4 col-sm-4 col-xs-12 product-category relative" style="border: 0.1px solid #d2d2d2;">
                <div class="image-product relative overfollow-hidden">
                    <div class="center-vertical-image">
                        <img src="img/{{$item->image_url1}}" alt="Product">
                    </div>
                    <!-- <a href=""></a> -->
                    <ul class="option-product animate-default" style="">
                        <li class="relative" wire:click="add_to_cart({{ $item->id }})">
                            <a>
                                <i class="data-icon data-icon-ecommerce icon-ecommerce-bag"></i>
                            </a>
                        </li>
                        <!-- <li class="relative"><a href=""><i class="data-icondata-icon-basic icon-basic-heart" aria-hidden="true"></i></a></li> -->
                        <li class="relative">
                            <a href="/detail/{{ $item->id }}">
                                <i class="data-icon data-icon-basic icon-basic-magnifier" aria-hidden="true"></i>
                            </a>
                        </li>
                    </ul>
                </div>
                <h3 class="title-product clearfix full-width title-hover-black"><a href="#">{{$item->designation}}</a></h3>
                <p class="clearfix price-product"><span class="price-old">$60</span>{{$item->priceUT}}</p>
            </div>
            @endforeach
        </div>
        <div class="d-flex justify-content-center">
            {{ $lesproduits->links() }}
        </div>
        <!-- End Product Content Category -->
    </div>

    <!-- Content Category view List -->
    <div id="viewlist" class="col-md-9 relative clear-padding" style="display:none;">
        <div class="bar-category bottom-margin-default border no-border-r no-border-l no-border-t">
            <div class="row">
                <div class="col-md-4 col-sm-4 col-xs-4">
                    <p class="title-category-page clear-margin">Alimentation</p>
                </div>
                <div class="col-md-8 col-sm-8 col-xs-8 right-category-bar float-right relative">
                    <p class=" float-left">results</p>
                    <a class=" float-left active-view-bar" onclick="showgird()">
                        <span class="icon-bar-category border ti-layout-grid3"></span>
                    </a>
                    <a class=" float-left" onclick="showlist()">
                        <span class="icon-bar-category border ti-layout-list-thumb"></span>
                    </a>
                    <a class="float-left" onclick="showCartBoxDetail()">
                        <img alt="Icon Cart" class="position-relative" src="{{asset('img/icon_cart.png')}}" />
                        <span class="position-absolute top-0 start-100 translate-middle badge rounded-pill" style="color:#fff;background-color:red;">
                            {{$nombrecart}}
                        </span>
                    </a>
                </div>
            </div>
        </div>
        <!-- Product Content Category -->
        <div class="relative clearfix">
            @foreach ($lesproduits as $item)
                <div id="{{$item->id}}" class="col-md-12 clear-padding border no-border-t no-border-l no-border-r product-category bottom-margin-default product-category-list relative">
                    <div class="image-product relative overfollow-hidden">
                        <div class="center-vertical-image">
                            <img src="img/{{$item->image_url1}}" alt="Product">
                        </div>
                        <a href=""></a>
                        <ul class="option-product animate-default clear-margin">
                            <li class="relative"><a href="#">
                                <i class="data-icon data-icon-ecommerce icon-ecommerce-bag"></i></a>
                            </li>
                            <li class="relative"><a href="#">
                                <i class="data-icondata-icon-basic icon-basic-heart" aria-hidden="true"></i></a>
                            </li>
                            <li class="relative"><a href="/detail/{{ $item->id }}" >
                                <i class="data-icon data-icon-basic icon-basic-magnifier" aria-hidden="true"></i></a>
                            </li>
                        </ul>
                    </div>
                    <div class="relative overfollow-hidden info-product-list">
                        <h3 class="title-product clearfix full-width title-hover-black"><a href="">{{$item->designation}}</a></h3>
                        <div class="clearfix ranking-product-category ranking-color">
                            <i class="fa fa-star" aria-hidden="true"></i>
                            <i class="fa fa-star" aria-hidden="true"></i>
                            <i class="fa fa-star" aria-hidden="true"></i>
                            <i class="fa fa-star-half" aria-hidden="true"></i>
                            <i class="fa fa-star-o" aria-hidden="true"></i>
                        </div>
                        <p class="clearfix price-product"><span class="price-old">$60</span>{{$item->priceUT}}</p>
                        <p class="intro-product-category">{{$item->description}}</p>
                        <div class="relative button-product-list clearfix">
                            <ul class="clear-margin">
                                <li class="button-hover-red" wire:click="add_to_cart({{ $item->id }})">
                                    <a class="animate-default">Add to Cart</a>
                                </li>
                                <li><a href="#" class="animate-default"><i class="fa fa-heart" aria-hidden="true"></i></a></li>
                                <li><a href="#" class="animate-default"><i class="fa fa-signal" aria-hidden="true"></i></a></li>
                                <li><a href="#" class="animate-default"><i class="fa fa-search" aria-hidden="true"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="row">
            <nav aria-label="Page navigation example">
                <ul class="pagination">
                    {{ $lesproduits->links() }}
                </ul>
            </nav>
        </div>
        <!-- End Product Content Category -->
	</div>

    <!-- End Content -->
</div>

    <!-- Content Modal -->
        <div class="modal fade bs-example-modal-lg" id="quickview" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered" role="document">
				<div class="modal-content clearfix">
					<div class="modal-body clearfix">
						<div class=" relative clearfix">
							<button type="button" class="close-modal animate-default" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true" class="ti-close"></span>
							</button>
							<div class="col-md-5 slide-modal clear-padding-left col-sm-12 col-xs-12">
								<div class="relative slide-modal-big full-width">
									<div id="owl-big-slide-quickview" class="owl-carousel owl-theme relative sync-owl-big-image">
									  <div class="item center-vertical-image">
									    <img src="img/{{$url_image1}}" alt="Product Image Big Slide">
									  </div>
									  <div class="item center-vertical-image">
									    <img src="img/{{$url_image1}}" alt="Product Image Big Slide">
									  </div>
									  <div class="item center-vertical-image">
									    <img src="img/{{$url_image1}}" alt="Product Image Big Slide">
									  </div>
									  <div class="item center-vertical-image">
									    <img src="img/{{$url_image1}}" alt="Product Image Big Slide">
									  </div>
									</div>
								</div>
								<div class="relative thumbnail-slide-detail full-width bottom-margin-default">
									<div id="owl-thumbnail-slide-quickview" class="owl-carousel owl-theme sync-owl-thumbnail-image" data-items="4,3,2,2">
									  <div class="item">
									    <img src="img/{{$url_image2}}" alt="Product Thumbnail Slide">
									  </div>
									  <div class="item">
									    <img src="img/{{$url_image3}}" alt="Product Thumbnail Slide">
									  </div>
									  <div class="item">
									    <img src="img/{{$url_image1}}" alt="Product Thumbnail Slide">
									  </div>
									  <div class="item">
									    <img src="img/{{$url_image2}}" alt="Product Thumbnail Slide">
									  </div>
									</div>

								</div>
							</div>
							<div class="col-md-7 col-sm-12 col-xs-12">
								<div class="top-title-modal border no-border-l no-border-r no-border-t bottom-padding-15-default">
									<p class="title-product-modal">{{$designation}}</p>
									<div class="box-rank-modal">
										<div class="clearfix ranking-product-modal ranking-color">
											<i class="fa fa-star" aria-hidden="true"></i>
											<i class="fa fa-star" aria-hidden="true"></i>
											<i class="fa fa-star" aria-hidden="true"></i>
											<i class="fa fa-star-half" aria-hidden="true"></i>
											<i class="fa fa-star-o" aria-hidden="true"></i>
										</div>
										<p class="count-review-modal">{{$poids}}</p>
									</div>
								</div>
								<p class="price-modal top-margin-15-default bottom-margin-15-default"><span class="price-old-modal">$60.00</span>
                                <span class="text-red left-margin-15-default">{{$priceUT}}</span></p>
								<p class="intro-product-modal">{{$description}}</p>
								<div class="option-button-modal relative bottom-margin-default top-margin-default">
									<div class="relative box-num-product-modal ">
										<input type="text" name="num-product" id="num-modal" value="1" class=""/>
										<span class="ti-plus animate-default" onclick="plusBtnInput('#num-modal')"></span>
										<span class="ti-minus animate-default" onclick="minusBtnInput('#num-modal')"></span>
									</div>
									<a href="#" class="add-cart-modal left-margin-15-default animate-default">ADD TO CART</a>
									<a href="#" class="add-favor-modal left-margin-15-default animate-default"><i class="fa fa-heart" aria-hidden="true"></i></a>
								</div>
								<div class="relative category-product-modal">
									<p>Categorie : <span class="text-black">{{$test}}</span></p>
									<p>Fabriqué : <a href="#" class="text-black">{{$dateFabr}}</a></p>
									<p>Expire : <span class="text-black"><a href="#" class="text-black">{{$dateExp}}</a></span></p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
    <!-- End Content Modal -->
