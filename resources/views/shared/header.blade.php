<!-- Menu Mobile -->
<div class="menu-mobile-left-content menu-bg-white">
    <ul>
        <li><a href="#"><img src="{{asset('img/icon_hot_gray.png')}}" alt="Icon Hot Deals" /> <p>Hot Deals</p></a></li>
        <li><a href="#"><img src="{{asset('img/icon_food_gray.png')}}" alt="Icon Food" /> <p>Food</p></a></li>
        <li><a href="#"><img src="{{asset('img/icon_mobile_gray.png')}}" alt="Icon Mobile & Tablet" /> <p>Mobile & Tablet</p></a></li>
        <li><a href="#"><img src="{{asset('img/icon_electric_gray.png')}}" alt="Icon Electric Appliances" /> <p>Electric Appliances</p></a></li>
        <li><a href="#"><img src="{{asset('img/icon_computer_gray.png')}}" alt="Icon Electronics & Technology" /> <p>Electronics & Technology</p></a></li>
        <li><a href="#"><img src="{{asset('img/icon_fashion_gray.png')}}" alt="Icon Fashion" /> <p>Fashion</p></a></li>
        <li><a href="#"><img src="{{asset('img/icon_health_gray.png')}}" alt="Icon Health & Beauty" /> <p>Health & Beauty</p></a></li>
        <li><a href="#"><img src="{{asset('img/icon_mother_gray.png')}}" alt="Icon Mother & Baby" /> <p>Mother & Baby</p></a></li>
        <li><a href="#"><img src="{{asset('img/icon_book_gray.png')}}" alt="Icon Books & Stationery" /> <p>Books & Stationery</p></a></li>
        <li><a href="#"><img src="{{asset('img/icon_home_gray.png')}}" alt="Icon Home & Life" /> <p>Home & Life</p></a></li>
        <li><a href="#"><img src="{{asset('img/icon_sport_gray.png')}}" alt="Icon Sports & Outdoors" /> <p>Sports & Outdoors</p></a></li>
        <li><a href="#"><img src="{{asset('img/icon_auto_gray.png')}}" alt="Icon Auto & Moto" /> <p>Auto & Moto</p></a></li>
        <li><a href="#"><img src="{{asset('img/icon_voucher_gray.png')}}" alt="Icon Voucher Service" /> <p>Voucher Service</p></a></li>
    </ul>
</div>
<!-- Header Box -->
<header class="relative full-width">
    <div class=" container-web relative">
        <div class=" container">
            <div class="row">
                <div class=" header-top">
                    <p class="contact_us_header col-md-4 col-xs-12 col-sm-3 clear-margin">
                        <img src="img/icon_phone_top.png" alt="Icon Phone Top Header" /> Contact nous <span class="text-red bold">+243 824202032</span>
                    </p>
                    <div class="menu-header-top text-right col-md-8 col-xs-12 col-sm-6 clear-padding">
                        <ul class="clear-margin">
                            <li class="relative"><a href="#">Mon Compte</a></li>
                            <li class="relative">
                                <a href="#">FR</a>
                                <ul>
                                    <li class="relative"><a href="#">EN</a></li>
                                </ul>
                            </li>
                            <li class="relative">
                                <a href="#">USD</a>
                                <ul>
                                    <li class="relative"><a href="#">CDF</a></li>
                                    <li class="relative"><a href="#">EUR</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="clearfix header-content full-width relative">
                    <div class="clearfix icon-menu-bar">
                        <i class="data-icon data-icon-arrows icon-arrows-hamburger-2 icon-pushmenu js-push-menu"></i>
                    </div>
                    <div class="clearfix logo">
                        <a href=""><img alt="Logo" src="{{asset('img/01LOGO.png')}}" width="200" height="42"/></a>
                    </div>
                    <div class="clearfix search-box relative float-left">
                        <form method="GET" action="/" class="">
                            <div class="clearfix category-box relative">
                                <select name="cate_search">
                                    <option>Les Categories</option>
                                    <option>Agri-culture</option>
                                    <option>Alimentaire</option>
                                    <option>Elevage</option>
                                    <option>pêche</option>
                                </select>
                            </div>
                            <input type="text" name="s" placeholder="Entrez votre recherche ici . . .">
                            <button type="submit" class="animate-default button-hover-red">search</button>
                        </form>
                    </div>
                    <div class="clearfix icon-search-mobile absolute">
                        <i onclick="showBoxSearchMobile()" class="data-icon data-icon-basic icon-basic-magnifier"></i>
                    </div>
                    <!-- <div class="clearfix cart-website absolute" onclick="showCartBoxDetail()">
                        <img alt="Icon Cart" src="{{asset('img/icon_cart.png')}}" />
                        <p class="count-total-shopping absolute">2</p>
                    </div> -->
                    <!-- <div class="clearfix cart-website absolute" onclick="showCartBoxDetail()">
                        <img alt="Icon Cart" src="{{asset('img/icon_cart.png')}}" />
                        <p class="count-total-shopping absolute">3</p>
                    </div> -->
                    <div class="cart-detail-header border">
                        <div class="relative">
                            <div class="product-cart-son clearfix">
                                <div class="image-product-cart float-left center-vertical-image ">
                                    <a href="#"><img src="{{asset('img/3-1.jpg')}}" alt="" /></a>
                                </div>
                                <div class="info-product-cart float-left">
                                    <p class="title-product title-hover-black"><a class="animate-default" href="#">Sac de Riz</a></p>
                                    <p class="clearfix price-product">$50 <span class="total-product-cart-son">(x1)</span></p>
                                </div>
                            </div>
                            <div class="product-cart-son">
                                <div class="image-product-cart float-left center-vertical-image">
                                    <a href="#"><img src="{{asset('img/Broli_green_peas-02.jpg')}}" alt="" /></a>
                                </div>
                                <div class="info-product-cart float-left">
                                    <p class="title-product title-hover-black"><a class="animate-default" href="#">Boite Haricot</a></p>
                                    <p class="clearfix price-product">$35 <span class="total-product-cart-son">(x1)</span></p>
                                </div>
                            </div>
                        </div>
                        <div class="relative border no-border-l no-border-r total-cart-header">
                            <p class="bold clear-margin">Subtotal:</p>
                            <p class=" clear-margin bold">$85</p>
                        </div>
                        <div class="relative btn-cart-header">
                            <a href="/panier" class="uppercase bold animate-default">view cart</a>
                            <a href="#" class="uppercase bold button-hover-red animate-default">Annuler</a>
                        </div>
                    </div>
                    <div class="mask-search absolute clearfix" onclick="hiddenBoxSearchMobile()"></div>
                    <div class="clearfix box-search-mobile">
                    </div>
                </div>
            </div>
            <div class="row">
                <a class="menu-vertical hidden-md hidden-lg" onclick="showMenuMobie()">
                    <span class="animate-default"><i class="fa fa-list" aria-hidden="true"></i> Les categories</span>
                </a>
            </div>
        </div>
    </div>
    <div class="menu-header-v3 hidden-ipx">
        <div class="container">
            <div class="row">
                <!-- Menu Page -->
                <div class="menu-header full-width">
                    <ul class="clear-margin">
                        <li onclick="showMenuHomeV3()"><a class="animate-default" href="#">
                            <i class="fa fa-list" aria-hidden="true"></i> Nos categories</a>
                        </li>
                        <li class="title-hover-red">
                            <a class="animate-default" href="#">home</a>
                        </li>
                        <li class="title-hover-red">
                            <a class="animate-default" href="#">Boutique</a>
                        </li>
                        <li class="title-hover-red">
                            <a class="animate-default" href="#">Nikita market</a>
                            <ul>
                                <li class="title-hover-red"><a class="animate-default" href="about.html">A propos</a></li>
                                <li class="title-hover-red"><a class="animate-default" href="contact.html">Contact Nous</a></li>
                            </ul>
                        </li>
                        <li onclick="showMenuHomeV3()"><a class="animate-default" href="#">
                            <img src="{{asset('img/zget-free.gif')}}" alt="" style="position: absolute;top: -1em;left: 25em;">
                        </li>
                    </ul>
                </div>
                <!-- End Menu Page -->

            </div>
        </div>
    </div>
    <div class="clearfix menu_more_header menu-web menu-bg-white">
        <ul>
            <li><a href="#"><img src="{{asset('img/icon_hot_gray.png')}}" alt="Icon Hot Deals" /> <p>Agri-culture</p></a></li>
            <li><a href="#"><img src="{{asset('img/icon_food_gray.png')}}" alt="Icon Food" /> <p>Alimentaire</p></a></li>
            <li><a href="#"><img src="{{asset('img/icon_mobile_gray.png')}}" alt="Icon Mobile & Tablet" /> <p>Elevage</p></a></li>
            <li><a href="#"><img src="{{asset('img/icon_mobile_gray.png')}}" alt="Icon Mobile & Tablet" /> <p>Pêche</p></a></li>
        </ul>
    </div>
    <div class="header-ontop">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <div class="clearfix logo">
                        <a href="#"><img alt="Logo" src="{{asset('img/01LOGO.png')}}" width="200" height="42"/></a>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="menu-header">
                        <ul class="main__menu clear-margin">
                            <li class="title-hover-red">
                                <a class="animate-default" href="/">home</a>
                            </li>
                            <li class="title-hover-red">
                                <a class="animate-default" href="/">Boutique</a>
                            </li>
                            <li class="title-hover-red">
                                <a class="animate-default" href="#">Nikita market</a>
                                <ul>
                                    <li class="title-hover-red"><a class="animate-default" href="about.html">A propos</a></li>
                                    <li class="title-hover-red"><a class="animate-default" href="contact.html">Contact Nous</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<!-- End Header Box -->