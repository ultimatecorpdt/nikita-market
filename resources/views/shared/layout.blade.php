<!DOCTYPE html>
<html lang="en">
<head>
	<title>{{$title}}</title>
	<meta name="format-detection" content="telephone=no">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<!-- LINK -->
    @include('shared/linkup')
    @livewireStyles
</head>
<body>
    <!-- LINK HEADER -->
    @include('shared/header')

    <!-- PAGE BLOCK -->
    @yield('content')
    <!-- PAGE BLOCK -->

    <!-- LINK FOOTER -->
    @include('shared/footer')

    @livewireScripts
	<!-- LINK BOTTOM -->
    @include('shared/linkbottom')

</body>
</html>