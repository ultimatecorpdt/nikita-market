<script src="{{ asset('js/jquery-3.3.1.min.js') }}" defer=""></script>
<script src="{{ asset('js/bootstrap.min.js') }}" defer=""></script>
<script src="{{ asset('js/multirange.js') }}" defer=""></script>
<script src="{{ asset('js/slick.min.js') }}" defer=""></script>
<script src="{{ asset('js/owl.carousel.min.js') }}" defer=""></script>
<script src="{{ asset('js/sync_owl_carousel.js') }}" defer=""></script>
<script src="{{ asset('js/scripts.js') }}" defer=""></script>

<script>
    function showgird() {
        document.getElementById('viewgrid').style.display = 'block';
        document.getElementById("viewlist").style.display = 'none';
    }

    function showlist() {
        document.getElementById('viewgrid').style.display = 'none';
        document.getElementById('viewlist').style.display = 'block';
    }

    function showCartBoxDetail(){
        setInterval(function () {
            $('#quickview').modal('show');
        }, 10000);
    }
</script>