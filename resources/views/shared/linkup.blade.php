<link href="https://fonts.googleapis.com/css?family=Montserrat%7CRoboto:100,300,400,500,700,900%7CRoboto+Condensed:100,300,400,500,700" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="{{ asset('css/icon-font-linea.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/multirange.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap-theme.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/themify-icons.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/effect.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/font-awesome.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/category.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/owl.theme.default.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/owl.carousel.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/responsive.css') }}">



<link rel="stylesheet" type="text/css" href="{{ asset('css/cartpage.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/product.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/slick.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/slick-theme.css') }}">

