<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [
    'uses' => 'IndexController@index_page',
    'as' => 'index.page'
]);

Route::get('/panier', [
    'uses' => 'CartController@panier_page',
    'as' => 'panier.page'
]);

Route::get('/detail/{id}', [
    'uses' => 'DetailProductController@detail_page',
    'as' => 'detail.page'
]);

Route::get('/paiement', [
    'uses' => 'PaiementController@paiement_page',
    'as' => 'paiement.page'
]);

